S3561453
Nathan Power

Cloud Computing Assignment 2


“My Energy”

An app and services for tracking energy usage in the home by the person (who’s home,) not just as a household.

The system makes use of an iOS client app and geofence boundary notifications to efficiently track who is ‘home.’ This data is then combined with electricity usage data from smart meter feeds and weather information to give users a better understanding of their energy usage. 

