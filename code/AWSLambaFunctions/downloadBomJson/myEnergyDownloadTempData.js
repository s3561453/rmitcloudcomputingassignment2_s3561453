process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT']

'use strict';
var request = require('request');
var AWS = require('aws-sdk');

var doc = new AWS.DynamoDB.DocumentClient();

var config;
var url = 'http://reg.bom.gov.au/fwo/IDV60901/IDV60901.95936.json';


// adapted from http://stackoverflow.com/a/39612292
// author: http://stackoverflow.com/users/613198/rsp
// accessed: 19/10/16

exports.handler = function(event, context) {

    request.get({
    url: url,
    json: true,
    headers: {'User-Agent': 'request'}
  }, (err, res, data) => {
    if (err) {
      console.log('Error:', err);
    } else if (res.statusCode !== 200) {
      console.log('Status:', res.statusCode);
    } else {
      // data is already parsed as JSON:
      console.log('air_temp: '+data.observations.data[0].air_temp);
      //console.log('getConfig data:', JSON.stringify(data.observations.data[0], null, 2));
      getConfig(data, context);
    }
  });
};

function getConfig(bomData, context) {
  //console.log('getConfig data:', JSON.stringify(bomData.observations.data[0], null, 2));
  if (config) {
    saveData(bomData, context);
  } else {
    var params = {
      TableName: 'IoTRefArchConfig',
      Key: { Environment: 'demo1' }
    };
    // get details of where to save data from db
    doc.get(params, function(err, data) {
      if (err) {
        console.log(err, err.stack);
      } else {
        console.log('Retrieved config');
        config = data.Item;
        saveData(bomData, context);
      }
    });
  }
}

function saveData(bomData, context) {
    //console.log('saveData data:', JSON.stringify(bomData, null, 2));


    var ddbParams = {
        RequestItems: {}
    };

    var putItems = [];

    /*
    bomData.observations.data.forEach(function(observation) {
        //payload = new Buffer(record.kinesis.data, 'base64').toString('ascii');
        //console.log('Parsing new observation: ', observation.sort_order);

        //var sensorEvent = JSON.parse(payload);

        putItems.push(
            {
                PutRequest: {
                    Item: {
                        aifstime_utc: observation.aifstime_utc,
                        local_date_time_full: observation.local_date_time_full,
                        apparent_temp: observation.apparent_t,
                        air_temp: observation.air_temp,
                    }
                }
            });
    });
    */
    var observation = bomData.observations.data[0];
    putItems.push(
        {
            PutRequest: {
                Item: {
                  aifstime_utc: observation.aifstime_utc,
                  local_date_time_full: observation.local_date_time_full,
                  apparent_temp: observation.apparent_t,
                  air_temp: observation.air_temp,
                }
            }
        });
    observation = bomData.observations.data[1];
    putItems.push(
        {
            PutRequest: {
                Item: {
                  aifstime_utc: observation.aifstime_utc,
                  local_date_time_full: observation.local_date_time_full,
                  apparent_temp: observation.apparent_t,
                  air_temp: observation.air_temp,
                }
            }
        });
    observation = bomData.observations.data[2];
    putItems.push(
        {
            PutRequest: {
                Item: {
                  aifstime_utc: observation.aifstime_utc,
                  local_date_time_full: observation.local_date_time_full,
                  apparent_temp: observation.apparent_t,
                  air_temp: observation.air_temp,
                }
            }
        });

    console.log('config.OutdoorTempDataTable: ', config.OutdoorTempDataTable);
    ddbParams.RequestItems[config.OutdoorTempDataTable] = putItems;

    doc.batchWrite(ddbParams, function(err, data) {
        if (err) {
            console.log('DDB call failed: ' + err, err.stack);
            context.fail(err);
        } else {
            context.succeed();
        }
    });

}
