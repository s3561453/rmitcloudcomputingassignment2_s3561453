CC assignment 2 app

Home energy usage

Summary
The cloud system collects and analyses data from the home and it’s occupants, which is combined with publicly available weather data, to give people insights into their energy usage.

What valuable insights could this provide?
 - how much energy you personally use (ave. consumption when home)
 - average household energy use for max and min temperatures

Data sets
 # inside house temp (1/min)
 # outside temp (BOM) (24/day?)
 # house member home t/f (1/min)
 # gas usage (1/min)
 # electricity usage (1/min)


— measure electricity use in real time
- measure gas use in real time
 - photograph -> OCR -> meter value

- track location of house members using mobiles

+ focus on the cloud system over UI

- track house 



